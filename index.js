/**
 * BÀI 1: TÍNH TIỀN LƯƠNG NHÂN VIÊN
 * Input: Lương 1 ngày = 100.000, số ngày làm: 7 ngày
 *
 *
 * Process:
 * 1: tạo biến chứa lương 1 ngày, số ngày làm và gán giá trị
 * 2: Tạo biến chứa lương
 * 3: Tính lương: lương 1 ngày * số ngày làm
 *
 *
 * Output: 700.000
 */
var luongMotNgay = 100000;
var soNgayLam = 7;
var tongLuong = null;
tongLuong = luongMotNgay * soNgayLam;
console.log("Tổng lương =  ", tongLuong + " VND");

/**
 * BÀI 2: TÍNH GIÁ TRỊ TRUNG BÌNH
 * Input: Nhập vào 5 số thực: 1, 2, 3, 4, 5
 * 
 * Process:
 * 1. Tạo các biến chứa 5 số thực
 * 2. Tạo biến chứa giá trị trung bình
 * 3. Tính giá trị trung bình: tổng của 5 số thực / 5
 * 
 * Ouput: 3

 */
var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;
var gttb = null;
gttb = (a + b + c + d + e) / 5;
console.log("Giá trị trung bình =  ", gttb);

/**
 * BÀI 3: QUY ĐỔI TIỀN
 * Input: Giá 1 USD = 23.500 VND; Số USD = 10;
 *
 * Process:
 * 1. Tạo biến chứa giá 1 USD, số USD
 * 2. Tạo biến chứa Số tiền quy đổi
 * 3. Số tiền quy đổi: Giá 1 USD * số USD
 *
 * Output: 235.000 VND
 */
var giaUSD = 23500;
var soUSD = 10;
var quyDoi = null;
quyDoi = giaUSD * soUSD;
console.log("Số tiền sau quy đổi = ", quyDoi + " VND");

/**
 * BÀI 4: TÍNH DIỆN TÍCH, CHU VI HÌNH CHỮ NHẬT
 * Input: chiều dài = 10, chiều rộng = 8
 *
 * Process:
 * 1: Tạo biến chứa chiều dài, chiều rộng
 * 2: Tạo biến chứa chu vi, diện tích
 * 3: Tính diện tích và chu vi hình chữ nhật
 *
 * Ouput: diện tích = 80; chu vi = 36
 */
var chieuDai = 10;
var chieuRong = 8;
var dienTich = null;
var chuVi = null;
dienTich = chieuDai * chieuRong;
console.log("Diện tích =  ", dienTich);
chuVi = (chieuDai + chieuRong) * 2;
console.log("Chu vi =  ", chuVi);

/**
 * BÀI 5: TÍNH TỔNG 2 KÝ SỐ
 * Input: nhập vào số 10
 *
 * Process:
 * 1: Tạo biến chứa số 10
 * 2. Tạo biến chứa hàng chục, đơn vị
 * 3. Tách hàng chục, tách hàng đơn vị
 * 4. Tạo biến chứa tổng ký số
 * 5. Tổng ký số: hàng chục + đơn vị
 *
 * Output: 1
 */
var n = 10;
var hangChuc = null;
var donVi = null;
var tongKySo = null;
hangChuc = n / 10;
donVi = n % 10;
tongKySo = hangChuc + donVi;
console.log("Tổng ký số =  ", tongKySo);
